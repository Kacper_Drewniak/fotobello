exports.onCreatePage = async ({ page, actions }) => {
    const { createPage } = actions

    if (page.path.match(/^\/categories/)) {
        page.matchPath = `/categories/*`
        createPage(page)
    }
    if (page.path.match(/^\/clients/)) {
        page.matchPath = `/clients/*`
        createPage(page)
    }
}