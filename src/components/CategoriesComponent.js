import React, {useEffect, useState} from 'react'
import {Card, CardBody, CardImg, CardTitle, Col, Row} from "reactstrap";
import request from "graphql-request";
import ReactLoading from 'react-loading';

const CategoriesComponent = () => {
    const [categories, setCategories] = useState(null);
    const [load, setLoad] = useState(false)
    useEffect(() => {
        const fetchProducts = async () => {
            const {categories} = await request(
                'https://api-euwest.graphcms.com/v1/ck4z6xq9xl19h01dodyes8hkg/master', `
                  { 
                   categories {
                        id
                        title
                        titleName
                        images {
                          id
                          url
                        }
                        number
                        mainCategoryPhoto{
                        id
                        url
                        }
                   
                      }
                } `
            );
            setCategories(categories)
        };
        fetchProducts();
        setTimeout(() => {
            setLoad(true)
        }, 2500)
    }, []);


    return <>
        {categories ? <Row className="m-0 d-flex justify-content-start">
            {categories.map((category, i) => {

                console.log(category)
                const _category = categories.find(category => category.number === i + 1);

                console.log(_category)
                return <Col sm="3" className="hover-white" style={{margin: ".5rem 0"}}>
                    <a href={`/categories/${_category.titleName}`}> <Card className="border-0">
                        {_category.mainCategoryPhoto.url ?
                            <CardImg style={{height: "200px", objectFit: "contain"}} top width="100%"
                                     src={_category.mainCategoryPhoto.url} alt="Card image cap"/> :
                            <ReactLoading type={"bars"} color={"white"} height={667 / 2} width={375 / 2}/>}
                        <CardBody className="category-body">
                            <CardTitle className="category-title">{_category.title}</CardTitle>
                        </CardBody>
                    </Card></a></Col>

            })}
        </Row> : <div className="h-100 d-flex justify-content-center align-items-center"><ReactLoading type={"bars"}
                                                                                                       color={"white"}
                                                                                                       height={667 / 2}
                                                                                                       width={375 / 2}/>
        </div>}
    </>
}

export default CategoriesComponent