import React, {useEffect, useState} from "react"
import {request} from 'graphql-request';
import Gallery from 'react-grid-gallery';

const CategoryComponent = ({name}) => {

    const [category, setCategory] = useState(null)

    useEffect(() => {
        const fetchProducts = async () => {
            const {categories} = await request(
                'https://api-euwest.graphcms.com/v1/ck4z6xq9xl19h01dodyes8hkg/master', `
                  { 
                   categories {
                        id
                        title
                        titleName
                        images {
                          id
                          url
                        }
                      }
                } `
            );
            const category = categories.find(c => c.titleName === name)
            setCategory(category)
            console.log(category.images)
            const x = category.images.map(item => ({src: item.url}))
            console.log(x)
        };
        fetchProducts();


    }, []);


    return <div className="d-flex flex-column justify-content-center">
        {category && category.images && <Gallery className="gallery-react" images={category.images.map(item => ({
            src: item.url,
            thumbnail: item.url
        })).reverse()}/>}
    </div>
}

export default CategoryComponent