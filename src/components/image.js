import React from 'react'
import {Img} from 'react-image'

const Image = ({name,url}) => {

    return <Img src={url} alt={name} className="category-image"/>
}

export default Image
