import React from "react";
import Layout from "../components/layout";
import {
  Container,
  Row,
  Card,
  CardText,
  CardBody,
  CardTitle,
} from "reactstrap";
import { isMobile } from "react-device-detect";

import loadable from "@loadable/component";
const AnimatedCursor = loadable(() => import("../components/AnimatedCursor"));

const Opinions = () => {
  const data = [
    {
      name: "Adrianna Jonkisz",
      avatar: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAJF6VFh0UmF3IHByb2ZpbGUgdHlwZSBBUFAxAAAImVWMQQ7DIBAD77wiT/DiDbDPQRGtIlVNlP8fugR6iBHYGiyHd/u2a9+W8zpe+6eF5ZaloKYWK4CCoQhQIN2RJ0rDk5UM9SCTNzy0eiMxSfb73wJs7OjoEIx+PNj9L5Pr9tzqHQoLqzv9FVrspOfVWaXSZ8IPaPgt1kfELoEAABu+SURBVHiczZt5kGVXfd8/v3Pu8pZ+vU93T8++SDOjXWKRBAaMbTYjJyEBEhvjAKGSghRx4opDuVIxVYRUObFDVfjDcqUcUzFkgyQqkgJScYpUYhs5LFqQJYGQNJJGmn16f8u995zzyx/n3tfdowWJpcpnprv63fX8vud3vr/1CT/hIci8MdkdRpJbRdKTIskRRJZEzbRgWkFU0TAKhFU0nA/qTwd1j6LV/arVvaBrP9n5/QSGlfQ1qW29KzGtd1iT3uJQBFAFVUHRHROIn1VAVXc9J6gSQvUt1eIrIRT3gHvgxz3XHycAaZ50P9JKuh9KbHIzCiFoFLp+kwJhxw0aBF+DofVkBHaBEZQInyqq5TdDGP6B6vD3flyT/nEAkPXy6d/o5JO/JjCpIWCMiauHogE0BILqGAClAUawYlBREEFDwGstLIqv7xEAFVQAUTSES8H3P606+K0fdfI/EgALvT1/Z7oz98k0SRdc8BSjEZX3WGNwwVM5F1deQMSgqlErgiJiUQGnfjyNoHHlm52gNGDt3hoNKKr+Ge+3fjPo6N/+sDL8UADkNjux2Fv8TJ533xpUGZUFqbGUVYUGpZ0mqHqGlceHgEMJCohgBayxhHqL+B18EDWk1prmmAJS34+C1LyhiohgEFwYfaly6x+DcOaVymJf6Q37pvZ84NTea75qJLk2tRZQWkkCHiCQiGBN3M1S7/uGB4JCQCh9oAyeoIqo4muJd5NjFFhEEW0+CyL1XxJBUBRrkpPWdP4W+CdV3cM/MQCuWzz0z69fPvrbEoLt5BmViwIHDagPiIAIBA34oHgf4rn6fkVQUYxITYqNyLKDCrdHQ4rxuRHQGlpU47FEwIghEclT036PoOq1+j8/dgBuXDz6hwenFz+qqhTOMywKGk4PqtiaqcUYqspThYAL0bwFtCa/yAdBG/MnNdkJAQHZtgXNb1MLCGBqVIIqSRQaUwMUQYA8bb05EbNU+uLLPzYADvaWvtBLu7/onMf5QCtNybKUNDFkWU5/NGJQlDj1VJUjxA0ftWHH6mEaspN6SyjarKxE6cIOEjQSARCN4IgRvCq5tSS15ZDakghaAwFt23q1EXOo8KMv/SDZfiAJvmrh8B8enZp7v3OevocieErnKXwFIgSFvJXjAhgDpXOAYVCUFK5EUaqghBDwqrU5lLhF6neoUHNA3BYmcvxY/ZOoXHiF3ILRuBdEDD4EgkJiwIqJEoVoOQauf/eW2/joS8mXvNTJO/Yd+xc3zO99PwqJGDotS3DgjFJ6ZasY0a8qNgYj+qXDhUDwjpFzkKZkNprDYAxWBIPiNBC8gtnJ9I3YSjwReaIxgT7qPW1rMTX7o9GEGhGSRrNCILEWTPQpOkn3Iy74c6PQ/6evWAPecOKaD90yd+DfTPc69NoZo2FJVTi8Vza3hqRpirUJVXCUCmujgpX1LTaLkqGrKIKjcI7SewofhQhExo8rbWjoUVWuoj9qKOKVQZXcGPLEIvXqWgxKdLYEyBKLGENROQyRnUQEHwLr5cp7y1B88WUDcHTv4qk3LR59aM/CjF2an2HUH6CFQ1C88yzMT1MO+6gDxFA5z6hyXFzdYLMIDFzFU5fW2RiUFNZThihqheJUCbUd9/Xe9jD2DBlTo6mdACU1ccUtkIiQJhZ89DitUai3U2IFiCSsSO1kgdewtlpevkEJz10t6wuS4Mfuett/cAN3/OTxfVxZ3eLshSu02znPXVwjAw4dWqDTaXPowBK9JNC2ykRm6CQp892UhckO1y7PcnR5jl5msGJxlYuTN7YWxJBawSIYAlYEa3buSR2TZ6h9PyNxpYPXWjtq8pDatCDN/7FmSPQdWiL2SBlG/+kHAvD+d7z1o5Obo4+94Y23kmcp33nkSY4dPsDXHnqK+YMLHF6ew28VtNoJN732VnoTLXqZpZXCzGSLXAKJqzDFiKXZLou9DqtbBa1ul8lOl0sbWxgxiAExigUCZuzuCmCJzpQVSIggpbWVVFXSxGKtpdvJox+AwVoIIlgUUV8LpvU/ITfpqUrdE17dd3bKu4sEW3ne/tDbfvaTF549w4HlJc5fuISVFFXDZlA07bBw7THWHnqEqnTMHTyC23+IsL5CGPTZ2ljl4tPPMtzsE4LyxJnLPPT4eaxVUoVzK5tkNokEhkYGLx1oIEkTKjV4ifsahQQlsZa09v5qTxhrhcRYqtIRVEnFkCU5XTGUrsIZgw+eVEF843fApJ345OUw+vyLasDH/+5HPnHAuLefOHWSpaPHuHTmWVbWtmCizVrS5vT3TjNvhKWZCdqdhMPHj9XemTJYucTG+fNknTadbofgSp46fY7JmQlOHT/AVLfHk0+NqPAsTMwSnGdxIuP1Nx3n4OIcz672WXfl2BeyApk1SAAxhjxN2Ts9hQJTrRaZNaCB1CYgMHIlVfBYa6E2pACptYgGVJV2ms64EPqVVl9/HgCtPG//x3999z3Di2dTsoT5I8fZuHyRyyvrrJSOqj3JvsU5Xn1iP5krmN8zy94jR6nWV/CjEThHOejTaneY3Lsf6c1w+FW3ce3N13Po2BEe+NNzPH1hg1Mz++nJJGfXt3jLnQd502uvY3F+Eld5vvPMJVqZxYiSALkYJjJLYhO2ygprLHmecm59g06esnd6lnaaMCxKEptgEcQKpfeoCMbGoCs10XxmaUZishs3qs1/+bwt8OH3/9JHO+12Z3JpH364QZLmzOyZw7sRXZNw000nuSZPuO7IAoNzpzlw2+2YvEMSFKlGiHrEWB6970F+6gMfYHLPXkLN1lvnNzn39f/J/n1LnJo7yn0PPsY1BzN+4effyJ6FRc6dPctDjz8HYrACuU2xCLlRWknKRLvNaG2Dyxsb7JmZpgzKmfUBl7ZGHJmbZu/sDM571voDhkVBYlOcOlQNYgxYS5pnOOcJxu7NbfvDhR/+PtTuNcAv/7V3fVA1MDk/S1WMqAZbJGnGwkyPajAgPHQ/h2czuhMdFq+/mXx+gZDn2D17sd1p0s4USbvDIAhPPPznkGaoSUgmp3noz57gCkPuuPEW1q542guBX3zPnRw6dg0Yw2g4YnZmklQT2jYhM5bEGrIkwxhLK004MN1DjGVrOCRLEzweDJxZ3+DxK2tc7A/Isoy5qUlm2jlz3R4zvS7WCqWrKCqPQ8ispZNNfHCXBtx+682vO3X08PX4iqnlA5Sbm1SuxOQ5s7M9ljZG3HT7rSxdcw3tPQtgLSEESHPIDSoGRkPy3hSd2Vme+e73uPYNb0baXXzp+OI/+TK33n6Srcf6nHoL3NRd4PVvegOdqTn662s88dSzPPbsJebblplOCw2BsqxQH1AxrG0NmGi36OUpW0UBYjB1WJ1ZS5JYBmVFWVXk1jKRZxSjPs4raZqyMSpiBKqKZClgX5fa7PrKlw8nAHf99BvfhXOosSDK7KFjiBHarmRx7wJLC3Mcv/N1yMxsk4pBUwNiCc4hicV0J7DdDnNL85w9fw5chdtY5bFvP06PlJ+76wizR1JcKMizFt2pGdI8p9WdwKQJjz55kYluxsJUj7YFV3rWNweMXEVRKZUO6WUZQ+cofYxCXQgMy4pWljPR7jIqBjhgrT8gq02iekdmDCPvqHxg5D0qYCR7F9QA3HHiyM8HV2KSFJC4b4yhPbvA4VffSdqeQLoTSGJRkbhxfG2WrEVs5FKTtelM9ej0N1k5dwaMcOWZR/n1//5ukpalHI3ATtLqTmGzDDca4pxjZbPPwmyPrSJjutdmsddhqp0yHA5Zu7ROr5vy1OUt7n3yIu2JFCuCqxMoRQhcGmzSsTlTeU7wFV6UQgRCIMeQGxj46F57rfOPZO8APmUzmyyf7LV/64brTuCDQ72LHlRisWmK6UThSdIorAh4j3hFiyKGqq6AoqQshpSjEXm7RX99hd7sDBNTbbKJLkEDNklJ213S3hSC4KqCx773Xe67/zHmJjvM9VocWZojM8LMZIsDe+c4uDjFqaPLvPHmY5xYmOCr33kUa1MSYwi1QIJQBs9GMaKVpFgx46g0hEA3yygrRwk4mjRbcsD7rd+27bz71vvu33jvkb0Zhw/uixoeQozcRgOMD3HFVSE4KAp0WESXU30EoxjiyxHlaEBAaU9OMTEzS/ABRRnW5tHkOTZvgSpb61f43iOP8MADD9PtZiRGWN4zz77FaSwBYw3WCMuLc5y66XoOX3stywtz/NShRf79vX9Mz05wZVSQpLbOGsfweLMqcD7QzlIAfB1Rdtst1kfF7uxUKP+37XWm32cm2m/81rceZ3Y6o9PNSYyQ5S2MgHoHZYW4CikdWpWYEJDKgXOwtcXwyiWGm+sMBluUVUWnNwXW4kOgKEaUZYHYBFcWjIZ91lcu8dD9D/Lc2SvsWZwnNYY8M1x76hidiS5lMaKqKkRhbn6SpaPXMHntjUzvP8Cx48d412tu49Nf+i/8/b/0Fs48c45LgxFliDYtrSPAUeWwNsH5mJUqgiNJUkbObafhtHrIJrb90TRJr1sNnscePcPTTz3BU0+fxhUDpie6ZFmOIATvKfqbWO9xwyF+MMAXBf3NFYpiiPcVQcBYg/eByjlC7ZBkrS5iLTZJGQyHfPtbD4KkHD5yiG63g3jPgcOHmFtYQDSQZZbNjS0uXVmnkycsHDpAa3aWZHIakpSFA/t438+8id/51G/xS3e9hesWZtg31WY0KDm/VVJ6RytJCCg2s/gAIxfqWkWdkVJFCWdsnk1+HEn2plboV46zlwc88cwVvvGN7/LEd7/Dvr1zTPZ6eO8RI1SuZNjfoBj1qVwVkxdZiqQZWZ6T2BQVIU1TTJKSZy0U2Or3eeyxJ/iTr/0ps9Oz3HjLLXQ7XQYbG/SmeswtLGKNibG8q9i4ssalcys8/eQFzj7+MINzpzFVRTvPQAyTk5P8jb/+Hh5+9FGWDy1jshQ/GPG2O05wcv8eHnj8CnnLMHAOrKVSZVg6rDFxN8c8w5rN08lPqKEX9cJwfmPIpfU+z61XXD43YrHt6HWVUX+DajBisLmBqyowgkkzbJoBgrEJYhPEWAgxieG9Y21tjaeffIoH7/tzgnOcPHGCa687SdLK8c7Tm56kPTGBSQyokBiDek+igb17Ztm3d5aDB/YjHmw5otpYxVgLxpJmGccP7UOHG7zq5DFuOL6MFAX7lpe46dgyp8+tslYUhBD3fMxWh3GWOagOZWn6SH+9KjuZsaxv9nn/m2/j+lPHWbl4kZuP7+eG44dozy0weeQ41WALHRUMNtfAOSrnyNIEm1g0WCSxuGKEcxWrV1Y5f/4iFy5cZGlhD/uWl2m32nRnptGg9NfXySfamLSFekeSpiDgipLh5gbgcUVJ2e+TWUu306bTnSCbnKI9t4cQPNVoSDkcMNjc5PKl8wRVetPTfPn/fpM/+fb3ObPa5/ELK2Q2Zl299/jgCcSQu/L+rJzaf6N33pvvn7vCL//sLXzyVz/MpSe/z5GbbyNbWOT33/5m3vuZu5m64WbEGEKocGXJsFJGz53mzne+h/fc9Q5uOHGU+akeIXg2V7e4eHGVNE8xWcYz5y4yKioW5meZnezSzTKOHz7A5GSvzvYmpGnKqBqxcuESKytXSESZnZ1lYc88U5NTdKamsa0OtpWDKlW/T1UUuOAZ9vtsrK/iXEVQz3rp+Juf/Hesbm7EbJKJ5TiL4IOPvkC0dity5MANXoKaFgbtD3l07XGWmeaf/cMP8fa/chej049y9oFvsXznTzN94nqC9yR5ly21bDxyH6fe/T5gEaiAPpPMMZ910ER5btCnpKw9bgFG9U8BwCH20em1SbzDD8CQcPS6WV538xFOXrOfg/v30WpFr7E7N4+xSV2JUNQHKldRjYYMN1cZjkrOnb/IpbVVysrxuf/xDe5/dpXR1iZaO3c+hFhHIKBqUPUrsn/5VN9Y0wnBkVXQSxKcq3j44iVgld/9Bx/jZ26/jfk9exhubDC1Z4n+YMT61iZ3f+4LfO6eb7K41KGVJLTyDOeVUekoigIvUKmnrCpCoOaKGIOHoHhfr4wxiAlMtFr0spwjizO84abDnDy2zP4Dy0zv289oOCK4inx6D5ok2FablWeeZvXCOdY2NrBJwtz0FJ3JCb5273f4tX/135CkiJzCdoYZ6joFggZ3VhaWjj2XmnQ51iUEVzp6BjI1GFHOXlinYosPvukOZpeX+K9f+mMeHDwDwEF7lKk9bYzZruUFFYZVRRU8I1fiXNP80IQRMXWdW0tqDUYswVcxwyeCFWW+2+b6A3u4/ZZjJKmwvGeapf0HmF8+jMlyxCYEBDfcYri5hitKLl5eRX3ga/c9wqc+93lstg9bl+kQicHbVUODe0Rm5w99O7et25A6CUksYbvK0TGWvM7QPLYyhFDSzXImOymdJCG3KYLG0pUxFK6iXxUUVXSnsyzFGos10TVV1dgrEAJeAwYhEWEiywnqKSqHNcJMN2d5ssvh+Qluue4gJ08cZ+/BQ+QTEwz7fYwYNAT6/S0unD+HTVt8//R5njh/md/5z18gb+9Hg6+Zv0m9X514Bw3F1xLnqtOJyW+LFdgwzqpmacLQOTYqR9skHJ7p4DXH+UDhHIOqIugQa5r8uxLUM91qMT0xgTExLeV9oHQuVoY1Ph9jML42S6L0y4JenqJpwpXBkAtFwcqgoNNt068MmmSUpWflzLOcefYsBM+VjT5PPH2eflFxZnWLL/2/R4ERrdYyQX1dUXiBITtP+NPS6c59qtue+cfjUnbMyI/9a4Ciqgg+kNuEVpLGzo0m/ew9SRKDExEYlRWlq/BojNfTBFWNTlNTDlIltTaGGARyETKEdp6xOixYr8tdrlII68AA2MfS1CR5GlhfrVjzG0Bak69gMzsOWRDB1KRXU+YLohH8xq8nVTW6P7S2a/BNG4vswDDPErxXRkVF6RxpkhA0xG4PFJwj1KrdFD0aVnDqMTY6OaphPMEqKEYiPdUMgCuKcX+QFSFrWUTmUJ1FMVwpt3BDMCm08jYaPEIeYQyx0Nqssg9hLPiuRd+1B9z9iatG93rV7dwYGktVUm8HFYJTsiyNpaeyZFgWbDcrCLXYiDGxEAEQAgGlDB6jIWZjROrnx+0SxIxL3lbAqyeYpO4RAud9nXMwGAkYDaQmlsQ1AHWbjQaNlrYOiOp2tHEvTdNc8nwAynuNqj9bVqNHvIaaoBRf12JC2G5qCurxGsizlCRPY2mrvl6D4gP4EF8enI+sWxNvUwZvkhEyrofruBxeaSCojBujqLO6casJ3ke9FIj1RWMih3hPCD6CQC148/za3L2Q8BrKrwODBMBXg68kNr3OU3dw7ABQ6wmp8yBQqWKsJWsZRqOqrgs06h3BoNaMpikqyhPVs9E2qYFohIUYuzesTQi4GkDvXVTvsSkF9bHupxpLYCE0BbRteaU5L/I8K6BafJVaYaiqwT1RNX3TqlWvTGDcBUJcZQL4utSS5ZELdIwWdbipY+CoGx+2ExGxzu91GwQD5NZSBYnHEai7QmL6SsZNE7ELwuwSvlko6vey8+/m3NUjFPeMAQi+/Lpz5cPNdX6HAI0mh0Bd0Y3ANCuZ5snYiWl6fpq4O+69qIrNvLZX38R0lgjT3S5OpOaMMAZNRLDWxuhvx2iqPlovzAsK+BIjqn9sphpzX1n2P9tMvuH/UK+oNoDUXVlamzIniqiQZE0pctsCNAJvr37THhOfbkRpJym9NKFfjuhXDmOT+npTx+sB71wkw2bhamuj2uz5HYi8bACGn23+3nGrtNu9pcsiSUeJgkHDJdt9ebCtUuPjtWC+aib3/Bk1PZ8CGIkFzTQxOBfwEgl054qO/fUdDBaj0W2LsmvlRTDGRg9QBHkR9Vf159RdWm4+mx2nhlXR/3Ro1FiUIBonhxKaPp4dD2y2SbM+sqPWPO4OkzoWgFjfJkZkaoRB5XAa8LWmjGv6suMZNaGiMS1HbW53TT1OZhusxgGquapZuHis/+mdt129VO3WxN4ziJmjZmgjwk4Mpd7HQcAE8HXfX9BoPXwZ8D6g5vnMK82PbANpTHSSak6P/T2NcKqgEbCw41mxFzlulp3vaAixacvd+Z74OHda3eWjO+d0dYOEQ3Ursa13NrGT1CUldnwe9/Zsl+0x2pzfNqG7hBcZR4wNEkaSyBsSXWJp7lNFxIzfq+i463T8ghf27ep37Xb/TO0TBLfx92B3g8QL0kfanvsjk7R+bqfq7Hj6LkAApC5OeFFMiKoXXOwH2h7Nugq1uzYGsxGaunO8MYMSHI0/MXZuNLp7Eaznh7hxiqY24eNXgxb3BL/6V5937Qs+wCSnss7CQ2LMy2qkNCFyRKMJCjEx6hUXAmjtvtaLakxj13UXltp0hGpDmg3ZRgvTODYvORQEE1357UNrWl28gZfbJIWGyxr8s0na+cu73KsXgktrgoTtpmZtiErGtzS9fVL39I1/79Cypuyo7DpMk6wZu7lcdcGOIdbu0gwRIbi1X4Hqz17o+hddYQ3V/SBdsa3Xxwc1rBsZWXcYqe0O7h0zrklIiAGVorVXF1HSxhNsUkVj6m/IpIZiLKe8RFi3PUzdP9iM4Dd/Ex3e/WLXv6SKB1/8kZjkmLHZzdDY/Xo6osQer9rvJyDasPfYAaz3c+SFpgV6LJJItNnsQE50G0jlqvMvMcaEvMNf8YO7CVsff6nbfuAeD254j7HZ9cZm16vq7snW+8JsO/1QszpI3fAUhTY7nCaVq0AY7+vawQnSeMOI7AixX2yoMM7Q1I3HGgaf1bDxt3+QfC+L5IIbfBGxx2yS39zMJGqtGb9TxyFuVHmzC6hmC9V2W8zzBJJmvzRgyg5QqMnvRVAYe431I0IY/B4vQ3h4Bd8XCG54jyodY/PXN/F8s6JjMXWbtaUGZSxGrf0SOZqGAGUM0NWs19zItgd19bnmvNm+QMPmJwib/+jlyvWKvjGivvhfqH/aJO13iohtMkLPGzu9R9n+POYDqZ0VrdNijebsekSdOGmW9UVH7eSo38Cv/Qo6+N1XItMr/s6QhuqB4AZfEJNcK5Icu3riYz2u4/wmetxlDusRQ2MThTU1lwhIUvPH1Wr/go4ZqB99Cb/yC1Dd+0rlecUAAKDhSqgGn1f1563NXytiuuMJbkv6PIO+S1+Cbu97qM1h3McmSWqfQrEG0izBWqnzIDr2hBX/DH7zV1U3fwN044cR5YcDoB4aqm/7cvMzinpMdpsguTQ2XXevdrT52/eKacwn9bZonAbBJLGYokFJ8wTqWEBEsInBSLgU3Nan1K+9G9yDP4oML8PAvuyRmnTiIzbtfkhsdvPV8YI2mWeN4XUTPI2/U9ZouxFsmkYPs6pIEovWKW7vym96P/yD4AZ/ob46+/yHmuw1Jmm/S2zrHcamtzTHd+bojLLtQodmKiECkCQx/FVFffkt9aOveDe8R0P1F/rL0y8yzLzY7A4x6a1ikpOIPQJ2ScROq9JCgkrQUSCsqvrzgp4W/KPBl/erL37iX5///zeBaUawNi8GAAAAAElFTkSuQmCC",
      description: "Miałam przyjemność współpracować z Natalią już kilka razy. Natalia jest niezwykle profesjonalna. Podczas sesji potrafi świetnie poradzić sobie z małymi..."
    },
    {
      name: "Violet",
      avatar: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAZiS0dEAEUAWgBkQqRiYQAACGtJREFUeJztW3tMW9cZ/3wvflwbY4ONAYMxYDCvQSAhpNAkJVPWJEvbKdsibZpWrdVejdRVq7RN/WOJVGnTuk39o5PaKpraruo2iUwjaHk1TaqQptAECI/EGDAGjI0xfsD18/rBtfcHcMGJwQ/uvU3W/SRL57vne/zO53PPPffcczjAMLh8vlwiUzwhzpM3i3IkNZhIXM7HhIVZPJ4UQVABAMSi5EowEg4vh4MBG+Hzzvg9uN6z5BpyO+19K5EwziQ/DhNOc/LkexWqshNypeqYOFfWtBNfbpdjwGk1X7KbZ7t8+NIwXRzXQVsCOBwOt6Sq7qVijfZFca5sF11+N8PtcvRbjRPvWabG36XL544TwEEQXnndrtfUtQ2vZnF5OXSQSoZwkHCYxu+/OTs28oed+tpRAoora35W2bjndZ4AU+yUSCYg/L454+jg6YUZw98y9ZFRAoRiSXVNS9tbsqKSpzMNTCfsFlP3+EDvy6GA35yubdoJKCqv+lHdvgNnEQThpmvLJFYiYc/Y7c9+sjg305mOHZqOclXT3je0u/f9icPhpGXHBhAU5ReUVpyEWCy2bLf1pGqXckPq2576UKWtO5UZPfaQV6A8xBNghU6r+WIq+ikloHH/1zuLyiq/vzNq7EEiy28RCEVqx/xcdzLdpAmob3vqw8ep8evIyZM38/iCgmQ9YdsEVDW1/lGlrXuJXmrsQSLL3xuLxaK43XZzK50tE6Cs0L6obW59gxlq7CGvQHnI714e87vxsUT1SKKLohxpbV3r/rPMUmMPta37z/IxYXGiuoQJqG5pe4uDII/coy5TcHl8aU1L+18S1T3UyJKq2lOl1fUvM0+LXYgk0tqA12P04Uujm6/H9QAERTFN457X2aXGHioTtC2uB1Q07D4jV6qOskeJXXB5/FwyEvG7nfbe9WtUD0BQFFPXNLz65VBjD+ra+DZSCVBV1Z1Cs7KE7FNiF3xMWFSsqf7xupy1XlBWaF+gK8hvX/k5tDR+jZI/OHceuq5cS8tHU10NnPnlKUCQ1f9ocnoWfvW7P9PCT1mhfWHeOPFXgLUeIJEr2rOlufW0eAeA20OjgAn41K9td/orZEc6ngSREKN86Can6KIH0vyCdpFEWg+wlgCFquwEbd4B4OrNXrA5nJSsKStN20dtpYYq+/wB+ODceVq4rUNRstpmBABArlR9k1bvAKCb2PjHxCIhfO+5YynbPtHcCIUKOSVPTM/SSQ0AAORK1TEAAISPCZXZktw6ugNcuH4DVkiSkvfuakjZ9vCBNkA4G4tVPV/008oNYPU2QFBUiEjkijbavQOAbtIIJouVkjXqEsAE/JRsqzXlVHnR6YIrN27Rzg8AQCovaEPEubJmRrwDwOCojioLMQxOPpN8jnVwXwsoZHmUvPlWohviXFkzIsqR1jAV4O/nL4A/QFDynobkd9qh9lbgrHV/kiThwvWUl/fShkgirUGwbHF5ctXMQARDYJgxUXK5qhikOeJtbaoryqiyaX4B7k8YmKIHmEhcjvAEWCFjEQCg5/YAVRbw+fDd40e21H36YDvI83Ip+e69hGsYtIGHYYUIl8eXMhnk4vUecCwtU3JTXfWWugf3tVDlABGEzgtXmKQGWVyeFEFQVMBoFIgfyNQlyrhn/GZUlaupsmHGBLjHyygvFEUFCVeE6MalT28CuTYn4PN48O2jhx/SefZwB+RKNr6t3uq/ywY1QKIkGWQ6yPDYOJitNkpuqNE+pNPesvE0di3j0H31U6ZpAUmSQSQSDjG6A2MdQzo9VS5VFkG5qiSuvnLT+4Ju0sgGJViJhHEkHCRsyVV3jn92XwIiGAIAAC43C577RgdVd/L4EZCIswEAIBqNwsc9zMz8HkSYIGwI4fPOsBEM93hhanaOkuurq6hya9PGe4J5wQb9I/fZoASE3zuD+D34OCvRAODzgY2BTVVUAPXa1VdejVpFXR/RTbBFB/xufBzxLruG2Ar478vXYAl3AwAAiqJwtOMA/ODEM5AtWl2JC4bC8K/LV9miA95l1xDidtr7WIsIAGOGjQFOW6GGXbUbEyOjaQ5sdmciM0aAOxf7kBARsPrcy8zOOTfhyo1bEI1GAQBAWaCAMpWSquu7O8IWDcAdi71RkgwgAABOq/kSW4HvDN+DeZsdAFYnRdKc1ckP7vFA53+YnfpuhtNqvgywtiRmN892sRYZVidGD0JvmGaTAtgtq21GAADcTnuvD1/WbW9CH85d/BhC4TAlR2Mx+OQz9oYi3LHY63fjOoBNH0as05Pvs0XAZneC0bSxo21h0c7a3B8gvq3UyiOColjHd37o/F//OhQiAgs3u/5BjbxUD4iSJGEav/fml0OLPZj08W2Mex2e0Q3/PhwKutilxB4In3fGNH4v7vtaXAKiJEkYRwdPs0uLPUwlaNtDCyIWg/5tl20+vS+ZjwHs5tku2+zURw9eT7giNDHQ94tYNEomqnscEQmH8PGB3oTbfhJuhIqEgs5gwG9RlKi/xSw1dqDr63ne7XJ8kahuy51g3mXXEIpmiaT5BU8yR415TI0OnrYY9O9sVb/tVrgl2/wnWLZYw9QRGKZhnhx7Z2q4/zfb6STdC+iwmLqyJdL6bAl9GyjYwLxx4n19/+c/TaaX0mbIxbmZc49TTzAb9O/q79xK2niANM4LOCymLhRFhdL8wkd6TDCODp4xDN/5dar6aW2HXbJZrxE+r0leXHr8UTs1shIJe+733XjeYtC/nY5d2o3w4UvDi6bpTlGORCsU52iSWzAPu8XUPdRz9dlMlvcy+hcj4ZBrYXbqoxARsElk+a1oFleUiZ+dgvD75iYG+16ZGul/jYxEPJn4+P/BSToIAXyFj84mwlfy8PRWeNSPz/8XLqkiuiJME9QAAAAASUVORK5CYII=",
      description: "Piękne zdjęcia, wspaniała pamiątka, to już nasza trzecia rodzinna sesja i na pewno nie ostatnia. Zawsze spotykamy się z ciepłym, serdecznym przyjęciem oraz..."
    },
    {
      name: "Anna Sroka",
      avatar: "https://lh3.googleusercontent.com/a/ACg8ocIhoo3kWFgZMUQ6oFx03nnK1BGERbSryNd5EQrprh5692ss8A=s64-c-rp-mo-br100",
      description: "Z Natalią spotykam się już kolejny raz na sesji czy to służbowej czy też prywatnej. Na każdej z nich czuję się komfortowo, atmosfera jaka panuje już od samego..."
    },
    {
      name: "Karina Kukla",
      avatar: "https://lh3.googleusercontent.com/a/ACg8ocKOciRq9NvLEBBcD87EQZBt_hZxCabPUj9esZ685EsoEE1JUA=s64-c-rp-mo-br100",
      description: "Skorzystałam już z sesji kobiecej, biznesowej i urodzinowej syna. Każda zachwyciła mnie i moich najbliższych. Natalia robi to z pasją co widać potem na zdjęciach. Na pewno jeszcze wrócę. Polecam"
    },
    {
      name: "Magdalena Mikołajek",
      avatar: "https://lh3.googleusercontent.com/a/ACg8ocK4dgixSQatddwN2P9mi2zCost1_5qA5nNzQmsjJi2OL6cN=s64-c-rp-mo-br100",
      description: "Świetny fotograf, wspaniała, pracowita kobieta! Polecam z całego serca. Dla nas zawsze znalazła czas, pomimo tego, że często umawialiśmy się na ostatnią chwilę."
    },
    {
      name: "Ewelina Pajda",
      avatar: "https://lh3.googleusercontent.com/a-/ALV-UjUs4VPvF9keYwtbjkN1Xar9yF6g6Y0D1Yxdv6vjnQ5r-gh_13Q=s64-c-rp-mo-br100",
      description: "U Natalii sesja to czysta przyjemność! Cudowne zdjęcia zachowane na kliszy to jest to! Polecam z całego serca sesje zdjęciowe w Fotobello! Za nami już kilka..."
    },
    {
      name: "Sylwia Nowak",
      description:
        "Polecam z całego serca! Za nami już trzy spotkania z Panią Natalia, każde kolejne jest lepsze! Cierpliwa, empatyczna i z sercem dla małych i dużych! ❤️",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocJ8Ftk7cEzILnk16oSlAjYTcSJxsoSIlALjJIh2oJzO=s40-c-rp-mo-ba4-br100",
    },
    {
      name: "Klaudia Honkisz",
      description:
        "A więc nasza przygoda z Panią Natalią zaczęła się od sesji z naszym nowonarodzonym synkiem. Wspaniałe podejście, przepiękne zdjecia. Byliśmy zachwyceni tym co stworzyła Pani Natalia. Stwierdziliśmy że jest tak dobra w swoim fachu że kolejne sesje, rodzinna z córką i synem a także nasz ślub muszą być wykonane właśnie przez Panią Natalię ♥️ Jest mega profesjonalna i zna się na rzeczy. Ma świetne podejście do dzieci, potrafi zachęcić do pozowania nawet najmniejsze szkraby. Jestem bardzo zadowolona te trzy sesje u Pani Natalii to na pewno nie ostatnie! Polecam z całego serca Fotobello! 🌷",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjVlGGxwqIQDZG3LZ_7zi8A8GDoEpfofOgNTJ3WhnQKCI3I=s40-c-rp-mo-br100",
    },
    {
      name: "Joanna Kucharska",
      description:
        "Przepiękne zdjęcia! Pani Natalia podchodzi do swojej pracy bardzo profesjonalnie. Dzięki temu mamy piękną pamiątkę na lata . Polecamy !",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocLOUEKy4tJcG5cIAGaVOmW7D5oKbr0dE4ori7R5ECnR=s40-c-rp-mo-br100",
    },
    {
      name: "Aleksandra Kozłowska-Mrożek",
      description:
        "Serdecznie polecam sesje zdjęciowe z Natalią! Jest miła, kontaktowa, punktualna i słowna. Jestem pod dużym wrażeniem tego, jak szybko otrzymałam zdjęcia - które wyszły prześlicznie!",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocJu_WlApFzMS_gtFV0d5mwRFemrSx2pq9FlwFyTSZuk=s40-c-rp-mo-br100",
    },
    {
      name: "Martyna Orlewska  ",
      description: "Pełen profesjonalizm, super zdjęcia, polecamy bardzo.",
      avatar:
        "https://lh4.googleusercontent.com/-94X6Ca8mIG4/AAAAAAAAAAI/AAAAAAAAAAA/O4z92UHALDo/s40-c-rp-mo-br100/photo.jpg",
    },
    {
      name: "Anna Bożek",
      description:
        "Kolejna sesja z Natalią za nami! I na pewno jeszcze wrócimy! Piękne zdjecia, miła atmosfera, cudowne podejście do dzieci 🙂 polecam z całego serca ❤️",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocJjQVNo5mQf28zqCirU3ofXOj2ntLD4m_D0Z1NoQ530=s40-c-rp-mo-br100",
    },
    {
      name: "Przemysław Wołowiec",
      description:
        "Pani Natalia robiła nam zdjęcia podczas ślubu i wesela jesteśmy bardzo zadowoleni ze współpracy z tą panią wszystko super bardzo polecam",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocLODf5lJuv5JFMxMC5BDbzPD9iIHvrJhehZLZQUxJ_w=s40-c-rp-mo-ba4-br100",
    },
    {
      name: "Justyna C.",
      description:
        "Polecam z całego serca ! Nie wiedziałam, że mogę czuć się tak swobodnie na sesji i że zdjęcia mogą być tak obłędne. Atmosfera, podejście fotografa i kreatywność oceniam na więcej niż 5 gwiazdek ale Google na więcej nie pozwala 🤣",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjUIeKREJAsf4xtnRVqqexK1llfRfDC3CgY9ebSonwWuJ-Rq=s40-c-rp-mo-br100",
    },
    {
      name: "Angela .BANG.",
      description:
        "Bardzo polecam! Rewelacyjna i komfortowa atmosfera podczas sesji i świetne podejście do każdego tematu sesji. Efekty przecudowne!",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjX4k0jTT3Aw6hQszM0pMO3E3P5WEmivvFJm3gFNstmONi0=s40-c-rp-mo-br100",
    },
    {
      name: "Barbara Folek",
      description:
        "Bardzo udana współpraca i profesjonalne podejście Pani Natalii, polecam serdecznie!",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjV7fDrqyasZ3Ls5SYnbXs1fl8j659GhdYx05kjjhdUdSg=s40-c-rp-mo-br100",
    },
    {
      name: "Ewelina Wojas",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocL3vwjqsG4Zhr5x3cHJpzwepNRmdna_L81ig-4oFsWi=s40-c-rp-mo-br100",
      description:
        "Super atmosfera podczas sesji  i fantastyczne podejście do dzieci sprawiają, że  czas sesji mija w mgnieniu oka! Efekty są natomiast super! Nawet ruchliwe dziecko zostanie uchwycone w super ujęciach 😃 Zdjęcia zrobione przez Natalie pozostają super pamiątka na lata 😃",
    },
    {
      name: "Elżbieta Koźmic",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocKuHHWcF6RXTp9WTncSClCh93JDsYx3bxm0Rr7HJ6-c=s40-c-rp-mo-br100",
      description:
        "Sesja bardzo profesjonalna i w przemiłej atmosferze. Wspaniała współpraca z dwójką naszych maluchów. Jestem zachwycona efektami pracy Pani Natalii. Zdjęcia z Chrztu będą pamiątką na lata.",
    },
    {
      name: "Mi Sz",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocKoYp_HwY0R_QfN2vphK_XaBp4l5eE9aYcLrgJbbnBy=s40-c-rp-mo-br100",
      description:
        "Serdecznie polecamy. Profesjonalnie wykonane zdjęcia ślubne. Dodatkowo doskonały kontakt oraz współpraca ☺️",
    },
    {
      name: "Kinga Gruszka",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjWJNrZVlA9ZrN78h3lJdshPdsGYyIPhIfP_vZ0py86kzg=s40-c-rp-mo-br100",
      description:
        "Polecam ! :)\n" +
        "Super atmosfera w studiu, zdjęcia przepiekne.\n" +
        "Pani Natalia to bardzo miła osoba z cudownym podejściemdo dzieci :)",
    },
    {
      name: "Ryszard B.",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjWWhjEsySfQPrikxHhKDA9n7lGRi7sFNZLfB7JRp9LwADAS=s40-c-rp-mo-ba4-br100",
      description:
        "Polecam, bardzo miła i sympatyczna młoda osoba, do burmuchów naprzeciwko już nie będę chodził",
    },
    {
      name: "Sylwia Biela",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjUeHsOWVI13JZqQYXGWfYPkIFUxukIjulVwTJe-zV6Ssw=s40-c-rp-mo-br100",
      description: "Piękne zdjęcia, anielska cierpliwość i ciepła atmosfera 😊",
    },
    {
      name: "Agnieszka K",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocKOgKCAf6bMCTE5aZaF92sEKZD2LGtKaB4SHMNRrbGl=s40-c-rp-mo-br100",
      description: "Piękne ujęcia i bardzo miła atmosfera 👍👍👍",
    },
    {
      name: "Martyna Orlewska",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocLPmoR7V9j-H9-4IFSTGG1FL1SpzaJQ6R5iu00f_klX=s40-c-rp-mo-br100",
      description: "Pełen profesjonalizm, super zdjęcia, polecamy bardzo.",
    },
    {
      name: "Ewelina B",
      avatar:
        "https://lh3.googleusercontent.com/a-/ALV-UjUF56w3icgdhSpwdyMhz72wQnvmte7ADNAC9xRwbxXFuGk=s40-c-rp-mo-ba3-br100",
      description: "Pełen profesjonalizm, pasja. Polecam 🙂",
    },
    {
      name: "AGAT K",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocIenLYsNzjYhuI_h84sYkdLlr795KBxhd91wXpmp_cv=s40-c-rp-mo-br100",
      description: "Świetna obsługa i piękne zdjęcia.POLECAM!!!",
    },
    {
      name: "Hupus6",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocL_y8x4iNAyjfzwsgKIhRu0FUBIXL3fdXoen_XA_f6D=s40-c-rp-mo-br100",
      description: "Polecam!",
    },
    {
      name: "Damian Wątroba",
      description: "Polecam!",
      avatar:
        "https://lh3.googleusercontent.com/a/ACg8ocLLUHaM9-91IRD0Re8AZ7AUUTkZl6ovfODLrobzeIun=s40-c-rp-mo-br100",
    },
  ];

  return (
    <Layout>
      {!isMobile && <AnimatedCursor />}
      <Container>
        {data.map(({ description, avatar, name }) => (
          <Row className="d-flex flex-column justify-content-center align-items-center">
            <Card className="w-50 m-1">
              <CardBody>
                <CardTitle>
                  <img src={avatar} alt={name} /> {name}
                </CardTitle>
                <CardText>{description}</CardText>
              </CardBody>
            </Card>
          </Row>
        ))}
      </Container>
    </Layout>
  );
};

export default Opinions;
